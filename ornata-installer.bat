@echo off
setlocal enabledelayedexpansion

rem Initialize a variable to store the last PHP path
set "lastPhpPath="

rem Get the PATH environment variable and loop through each path
for %%a in ("%PATH:;=" "%") do (
  rem Remove surrounding double quotes (if any)
  set "path=%%~a"

  rem Normalize the path by ensuring it ends with a backslash
  if not "!path:~-1!"=="\" set "path=!path!\"

  rem Check if php.exe or php exists in the current path to get the last one to use.
  if exist "!path!php.exe" (
    set "lastPhpPath=!path!php.exe"
  ) else if exist "!path!php" (
    set "lastPhpPath=!path!php"
  )
)

rem Check if a PHP executable was found
if "%lastPhpPath%"=="" (
  echo No PHP executable found in PATH.
  exit /b 1
)

rem Execute the OrnataInstaller.php script with the found PHP executable
"%lastPhpPath%" "%~dp0src\OrnataInstaller.php" %*
