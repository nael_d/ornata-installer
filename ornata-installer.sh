#!/bin/bash

# Initialize a variable to store the last PHP path
lastPhpPath=""

# Loop through each path in the PATH environment variable
IFS=":" read -ra PATH_ARRAY <<< "$PATH"
for path in "${PATH_ARRAY[@]}"; do
  # Remove surrounding double quotes (if any)
  path=$(echo "$path" | sed 's/^"\(.*\)"$/\1/')

  # Normalize the path by ensuring it ends with a slash
  if [ "${path: -1}" != "/" ]; then
    path="${path}/"
  fi

  # Check if php or php.exe exists in the current path to get the last one to use
  if [ -x "${path}php.exe" ]; then
    lastPhpPath="${path}php.exe"
  elif [ -x "${path}php" ]; then
    lastPhpPath="${path}php"
  fi
done

# Check if a PHP executable was found
if [ -z "$lastPhpPath" ]; then
  echo "No PHP executable found in PATH."
  exit 1
fi

# Execute the OrnataInstaller.php script with the found PHP executable
"$lastPhpPath" "$(dirname "$0")/src/OrnataInstaller.php" "$@"
