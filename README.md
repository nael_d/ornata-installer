# Ornata Installer
Ornata Installer is a command-line tool designed to facilitate the installation of the Ornata framework on your system.

## Installation
Ornata Installer is ready through Composer:
```
>_ composer global require nael_d/ornata-installer
```

This will get Ornata Installer ready through your system.

## Getting started
You are ready to install Ornata through `ornata-installer new` command:
* For Windows:
```
  >_ ornata-installer new [project-name]
```
* For Mac/Linux:
```
  >_ ./ornata-installer.sh new [project-name]
```
This will fetch the latest available version of Ornata to install.

To install a specific version of Ornata (if exists), you may pass the desired version as an optional parameter:
```
  >_ ornata-installer new [project-name] [--tag=VERSION]
```

## Version:
To check the version of Ornata Installer CLI, use the following command:
```
  >_ ornata-installer version
```
This will display the currently installed version of the Ornata Installer CLI.

In addition, to check the latest version of Ornata, use this flag:
```
  >_ ornata-installer version --latest
```
This will display the latest published version of Ornata.

## Documentation:
Ornata features an offline-ready built-in documentation system. Access it by running `ornata documentation` after installing Ornata to explore comprehensive guidance and resources.

## Licensing
Ornata Installer is open-sourced software licensed under the [MIT license](https://gitlab.com/nael_d/ornata-installer/-/blob/master/LICENSE).
