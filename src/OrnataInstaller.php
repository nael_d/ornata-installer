<?php

function color(string $color, string $text) {
  $colors = [
    "black"         => "\033[0;30m",
    "red"           => "\033[0;31m",
    "light-red"     => "\033[1;31m",
    "green"         => "\033[0;32m",
    "light-green"   => "\033[1;32m",
    "brown"         => "\033[0;33m",
    "orange"        => "\033[0;33m",
    "blue"          => "\033[0;34m",
    "light-blue"    => "\033[1;34m",
    "purple"        => "\033[0;35m",
    "light-purple"  => "\033[1;35m",
    "cyan"          => "\033[0;36m",
    "light-cyan"    => "\033[1;36m",
    "light-gray"    => "\033[0;37m",
    "dark-gray"     => "\033[1;30m",
    "yellow"        => "\033[1;33m",
    "white"         => "\033[1;37m",
    "default"       => "\033[0m",
  ];

  if (!array_key_exists($color, $colors)) $color = $colors["default"];
  return $colors[$color] . "{$text}" . $colors['default'];
}

class OrnataInstaller {

  public $dir;
  private $ds = "/";

  public function __construct() {
    $this->ds = strpos(($_SERVER['PHP_SELF']), "/") ? "/" : "\\";

    $this->dir = explode($this->ds, dirname($_SERVER['PHP_SELF']));
    array_pop($this->dir);
    $this->dir = implode($this->ds, $this->dir);

    error_reporting(0);
    require_once "progressbar{$this->ds}Progressbar.php";
    require_once "{$this->dir}{$this->ds}vendor{$this->ds}autoload.php";
  }

  private function args(array $args) {
    $args = array_slice($args, offset: 2);
    $parsedArgs = [];
    $currentKey = null;

    foreach ($args as $arg) {
      // Check if the argument follows the --parameter value format
      if (strpos($arg, '--') === 0) {
        // Remove the leading --
        $currentKey = substr($arg, 2);
        // Check if the argument has a value assigned with space or equal sign
        if (strpos($currentKey, '=') !== false) {
          // Split parameter and value by equal sign
          [$currentKey, $value] = explode('=', $currentKey, 2);
          $parsedArgs[$currentKey] = $value !== "" ? $value : true;
        }
        else {
          $parsedArgs[$currentKey] = true; // Initialize as boolean (no value specified yet)
        }
      }
      else if ($currentKey !== null) {
        // If a key is set, assign the argument value
        $parsedArgs[$currentKey] = $arg;
        $currentKey = null; // Reset key after assigning value
      }
      else {
        // no flags are passed, so we list all params in an ordered zero-based array
        $parsedArgs[] = $arg;
      }
    }

    return $parsedArgs;
  }

  private function output(string $message, bool $eol_before = false, bool $eol_after = false, bool $eol_normal = true) {
    echo ($eol_before ? "\n" : '') . "{$message}" . ($eol_normal ? PHP_EOL : '') . ($eol_after ? "\n" : '');
    ob_flush();
  }

  private function animation(string $text, bool|null $status = null) {
    $symbols = ['\\', '|', '/', '-'];

    if ($status === null) {
      for ($i = 0; $i < 8; $i++) {
        foreach ($symbols as $symbol) {
          $this->output("\r{$text}\t\t\t{$symbol}", eol_normal: false);
          usleep(50000);
          ob_flush();
        }
      }
    }

    if ($status === false) {
      $this->output("\r{$text}\t\t[FAILED]", eol_after: true, eol_normal: false);
    }
    else if ($status === true) {
      $this->output("\r{$text}\t\t\t[OK]", eol_after: true, eol_normal: false);
    }
  }

  public function handle(array|null $args) {
    if (count($args) == 1) {
      // $this->howto();
      echo <<<ascii
        \033[1;37m
          ____                   _          _____           _        _ _           
         / __ \                 | |        |_   _|         | |      | | |          
        | |  | |_ __ _ __   __ _| |_ __ _    | |  _ __  ___| |_ __ _| | | ___ _ __ 
        | |  | | '__| '_ \ / _` | __/ _` |   | | | '_ \/ __| __/ _` | | |/ _ \ '__|
        | |__| | |  | | | | (_| | || (_| |  _| |_| | | \__ \ || (_| | | |  __/ |   
         \____/|_|  |_| |_|\__,_|\__\__,_| |_____|_| |_|___/\__\__,_|_|_|\___|_|   

        Welcome to Ornata Installer CLI

        Ornata Installer CLI simplifies Ornata's initial setup
        by automating the installation of essential files and folders,
        preparing them for seamless first-time initialization by developers.        

        To create a new project with the latest version:
        \033[32m`ornata-installer new my-project`
        \033[1;37m
        To create a new project with a specific tag version:
        \033[32m`ornata-installer new my-project v0.0.0`
        \033[1;37m
      ascii;
      die(); // to prevent continue running this method that will fail in this case.
    }

    $command = $args[1] ?? '';

    if (method_exists($this, $command)) {
      $this->{$command}(...$this->args($args));
    }
    else {
      $this->output("`{$command}` task is not defined in Ornata Installer CLI.", true, true);
    }
  }

  public function getTerminalWidth() {
    switch (PHP_OS_FAMILY) {
      case "Windows":
        return (int) exec('%SYSTEMROOT%\System32\WindowsPowerShell\v1.0\powershell.exe -Command "$Host.UI.RawUI.WindowSize.Width"');
      case 'Darwin':
        return (int) exec('/bin/stty/ size | cut -d" " -f2');
      case 'Linux':
        return (int) exec('/usr/bin/env tput cols');
      default:
        return 0;
    }
  }

  public function new($project_name = null, $tag = null) {
    if (!$project_name) {
      $this->output("Error: No project name provided for installation.", true, true);
    }
    else {
      $link = null;

      if (!$tag) {
        // get the latest version
        $this->output(color("cyan","Getting the latest version ..."));
        $tags = current(json_decode(file_get_contents("https://gitlab.com/api/v4/projects/20682065/repository/tags?per_page=1")));
        $tag  = $tags->name;
        $link = "https://gitlab.com/nael_d/ornata/-/archive/{$tag}/ornata-{$tag}.zip";
        $this->output(color("light-purple","Ornata {$tag} is the latest version."));
      }
      else {
        // get a specific version if exists
        $this->output(color('green',"Installing `{$tag}` ..."));
        $tags = json_decode(file_get_contents("https://gitlab.com/api/v4/projects/20682065/repository/tags?per_page=100"));
        $version = array_filter($tags, function ($t) use($tag) {
          return $t->name == $tag;
        });

        if (count($version) > 0) {
          $version = current($version);
          // version exists
          $link = "https://gitlab.com/nael_d/ornata/-/archive/{$version->name}/ornata-{$version->name}.zip";
        }
        else {
          // version doesn't exist, let's output that.
          $this->output(color('yellow', "Failed to install this version."), false);
          die;
        }
      }

      $this->output(color('green', "Downloading ..."), eol_normal: false);

      // now we have the $link to download.
      // let's create the project's folder first.
      mkdir($project_name, 0777);
      $file = fopen("{$project_name}/ornata-{$tag}.zip", 'w');

      // now, let's retrieve the $link using cURL
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $link);
      curl_setopt($ch, CURLOPT_NOPROGRESS, false);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_FILE, $file);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
      curl_setopt($ch, CURLOPT_PROGRESSFUNCTION, function ($resource, $downloadSize, $downloaded, $uploadSize, $uploaded) {
        // print_r([$resource, $downloadSize, $downloaded, $uploadSize, $uploaded]);
        if ($downloadSize == 0 and $downloaded == 0) {
          // do nothing because it's been started recently.
        }
        else if ($downloadSize > 0) {
          echo (new \ProgressBar($downloadSize))->drawCurrentProgress($downloaded);
        }
        else {
          static $startTime;
          $startTime ??= time();
          $elapsed = time() - $startTime;
          $d = $downloaded <= 1048576 ? round($downloaded / 1024, 2) . " KB/s" : round($downloaded / 1024 / 1024, 2) . " MB/s";;
          $s = '';
          $o = '';

          if ($elapsed > 0) {
            $speed = ($downloaded / $elapsed); // speed in bytes
            $s = "• Speed: (".
              ($speed <= 1048576
                ? (round($speed / 1024, 2) . " KB/s")
                : (round($speed / 1024 / 1024, 2) . " MB/s"))
            .")";
          }

          $o = "\rDownloading: {$d} {$s}";
          $this->output($o . str_repeat(' ', max(0, $this->getTerminalWidth() - strlen($o))), eol_normal: false);
        }

        ob_flush(); // necessary for cli instant refreshing
      });

      // Execute cURL session
      sleep(1);
      curl_exec($ch);

      // Close the cURL session
      curl_close($ch);

      // Close the file
      fclose($file);

      // Move to a new line after progress completion
      echo PHP_EOL;

      // Output the success message
      $this->output(color('green', "Ornata {$tag} has been downloaded. Unzipping ..."));

      $zipArchive = new \ZipArchive();
      $zipArchive->open("{$project_name}/ornata-{$tag}.zip");
      $zipArchive->extractTo("{$project_name}/");
      $zipArchive->close();

      // Output the final success message
      $this->output(color('green', "Ornata {$tag} has been installed successfully."));
    }
  }

  public function version($latest = null) {
    if (!$latest) {
      $v = file_get_contents("{$this->dir}{$this->ds}version");
      $this->output("Ornata Installer version is: " . color('green', "`{$v}`"), true, true);
    }
    else {
      $tags = current(json_decode(file_get_contents("https://gitlab.com/api/v4/projects/20682065/repository/tags?per_page=1")));
      $tag  = $tags->name;
      $this->output("Latest version of Ornata is: ". color("green","`{$tag}`"), true, true);
    }
  }
}

(new OrnataInstaller())->handle($argv);
